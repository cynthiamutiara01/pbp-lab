import 'package:lab_6/screens/homepage.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'TEST',
      theme: ThemeData(scaffoldBackgroundColor: const Color(0xFF1B1D24)),
      home: const HomePage(),
    );
  }
}