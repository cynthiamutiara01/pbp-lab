from django.urls import path
from .views import index, xml, json

urlpatterns = [
    path('', index, name='index'), # Add index path using index Views
    path('xml', xml), # Add xml route using xml Views
    path('json', json) # Add json route using json Views
]