from django.shortcuts import render, redirect
from lab_2.models import Note
from lab_4.forms import NoteForm

# Create your views here.

def index(request):
    notes = Note.objects.all()  # Load Note model in index method
    response = {'notes': notes}
    return render(request, 'lab4_index.html', response)

def add_note(request):
    context ={}
  
    # create object of form
    form = NoteForm(request.POST or None, request.FILES or None)
      
    # check if form data is valid
    if request.method == "POST":
        if form.is_valid():
            # save the form data to model
            form.save()
            return redirect("/lab-4/")
  
    context['form']= form
    return render(request, "lab4_form.html", context)

def note_list(request):
    list = Note.objects.all()
    response = {'list': list}
    return render(request, 'lab4_note_list.html', response)
